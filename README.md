STEPS to Literacy Grant Project.

Built in Flex using ActionScript 3 (2010!)

Presented in AERA

The data collected was used in doctoral dissertations at Teachers College.

Students' every action (opening or closing a resource, focusing in or out of the writing field, taking a note) is immediately sent to the database with timestamp throughout the session.

Enables teachers to add new modules, resources to the modules, and create prompts.

Bilingual: English and Spanish


